package frc.robot;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.CAN;

import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.PWMVictorSPX;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;




public class Robot extends TimedRobot {
  //lift
  private TalonSRX lift;

  //mag/shooter
  private CANSparkMax wheel;
  private CANSparkMax wheel2;
  private CANSparkMax winch;
  private PWMVictorSPX ballsuccer;

  //tank drive vars
  private DifferentialDrive tankDrive;
  private WPI_TalonFX leftParent;
  private WPI_TalonFX leftChild;
  private WPI_TalonFX rightParent;
  private WPI_TalonFX rightChild;
  private double kDriveTicksPerRev = 2048;
  private double kWheelCirc = 6*Math.PI;
  private double kGearRatio = 3;
  private double kRevsToInch = (kDriveTicksPerRev*kGearRatio)*kWheelCirc;

  //air sys
  private Compressor compressor;
  private DoubleSolenoid dsol;

  //sensor vars
  private DigitalInput testLimitSwitch;
  private AnalogInput sonic0;
  private AnalogInput sonic1;
  private CameraServer camserv;
  private double disToWall;
  private double angToWall;


  //controllers
  private XboxController controller;
  private XboxController controller2;


  //toggle stuff
  private boolean aPressed=false;
  private boolean isExtended=false;
// Inversion
// MotorMagic

/*
Helpful sources:
Chief Delphi
Programming done right frc
StackOverFlow (ofc)
Logan Buyers
Head First Java
ScreenStepsLive
*/
private boolean doAuto = false;
  @Override
    public void robotInit() {
   
    //motors that aren't drive
    wheel = new CANSparkMax(8, MotorType.kBrushless);
    wheel2 = new CANSparkMax(9, MotorType.kBrushless);
    winch = new CANSparkMax(10, MotorType.kBrushless);
    ballsuccer = new PWMVictorSPX(4);


    //sensors
    sonic0 = new AnalogInput(0);
    sonic1 = new AnalogInput(1);
    testLimitSwitch = new DigitalInput(1);



    //cams
    camserv = CameraServer.getInstance();
    camserv.startAutomaticCapture(0);
    camserv.startAutomaticCapture(1);
    
    
    
    
    //air
    compressor = new Compressor();
    dsol = new DoubleSolenoid(2, 3);


    //tanksDrive
    // mid = new DifferentialDriveWheelSpeeds(leftMetersPerSecond, rightMetersPerSecond)
    leftParent = new WPI_TalonFX(4);
    leftChild = new WPI_TalonFX(5);
    leftChild.follow(leftParent);
    rightParent = new WPI_TalonFX(6);
    rightChild = new WPI_TalonFX(7);
    rightChild.follow(rightParent);
    tankDrive = new DifferentialDrive(rightParent, leftParent);


    //driveencoders
    rightParent.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor);
    rightParent.setSelectedSensorPosition(0);
    leftParent.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor);
    leftParent.setSelectedSensorPosition(0);

    // lift
    // lift = new TalonSRX(1);
    // lift.configSelectedFeedbackSensor(FeedbackDevice.QuadEncoder, 0, 0);
    lift =  CANTalonFactory.createTalon(1, false, NeutralMode.Brake, FeedbackDevice.QuadEncoder, 0, true);
    lift.setSelectedSensorPosition(0);
    


    //MAKE SURE GREEN CONTROLLER IS 1 IN DRIVER STATION!!!!!!!!!
    controller2 = new XboxController(0);
    controller = new XboxController(1);

    System.out.println("Let's Get This Bread");
  
  }

  @Override
  public void teleopInit() {
    Enabled();
  }

  @Override
  public void disabledInit() {
    compressor.stop();
    dsol.set(DoubleSolenoid.Value.kReverse);
    leftParent.setSelectedSensorPosition(0);
    rightParent.setSelectedSensorPosition(0);
    lift.setSelectedSensorPosition(0);
  }

  public void Enabled()
  {
    // compressor.start(); 
    // lift.setSafetyEnabled(true);
  }

  public void periodic() {    
    if(controller2.getPOV()==-1){
      stopShoot();
    }else if(controller2.getPOV()==0){
      shoot();
    }else if(controller2.getPOV()==45){
      //stuff
    }else if(controller2.getPOV()==90){
      shootLower();
    }else if(controller2.getPOV()==135){
      //stuff
    }else if(controller2.getPOV()==180){
      unsucc();
    }else if(controller2.getPOV()==225){  
      //stuff
    }else if(controller2.getPOV()==270){
      succ();
    }else if(controller2.getPOV()==315){
      //stuff
    }
    lift();
    winch();
    //dis to wall ang to wall
    disToWall = (sonic0.getValue() + sonic1.getValue())/2;
    angToWall = Math.toDegrees((Math.atan(((sonic1.getValue()-sonic0.getValue())/60)))); //60 = space tween sensors

    if(testLimitSwitch.get()){
      //switch is pressed
    }
    else{
      //switch is not pressed
    }

    tankDrive.tankDrive((controller.getY(Hand.kRight)*.6), (controller.getY(Hand.kLeft)*.6));
    tankDrive.feedWatchdog(); 

    if (controller.getAButtonPressed()){
      if (!aPressed){
        aPressed=true;
        if(isExtended){
          dsol.set(DoubleSolenoid.Value.kReverse);
          isExtended=false;
        }
        else{
          dsol.set(DoubleSolenoid.Value.kForward);
          isExtended=true;
        }
      }
    }
    else{
      aPressed=false;
    }
  }
  

  @Override
  public void autonomousInit() {
    for(int i=0;i<100;i++){
      shoot();
      Timer.delay(.01);
    }
    stopShoot();
    Timer.delay(1);
    for(int i=0;i<100;i++){
      tankDrive.tankDrive(.3, .3);
      Timer.delay(.01);
    }
    Timer.delay(1);
    Enabled();
  }

  @Override
  public void autonomousPeriodic() {
    if(doAuto){
      autoPeriodic();
    }
  }

  @Override
  public void teleopPeriodic() {
    periodic();
  }
  private void shoot() {
    wheel.set(-1);
    wheel2.set(1);
    succ();
  }
  private void shootLower() {
    wheel.set(-.25);
    wheel2.set(.25);
    succ();
  }
  private void stopShoot() {
    wheel.set(0);
    wheel2.set(0);
    stopsucc();
  }
  private void succ() {
    ballsuccer.set(-.5);
  }
  private void unsucc() {
    ballsuccer.set(.2);
  }
  private void stopsucc() {
    ballsuccer.set(0);
  }
  private void autoPeriodic() {
    System.out.println(rightParent.getSelectedSensorPosition());

    if(rightParent.getSelectedSensorPosition() >= 2048*3 && leftParent.getSelectedSensorPosition() <= 2048*-3){
      tankDrive.tankDrive(.3, .3);
    }else{
      tankDrive.tankDrive(0, 0);
    }
  }
  private double prevPos;
  private void lift() {
    System.out.println(lift.getSelectedSensorPosition());
    if(lift.getSelectedSensorPosition()>prevPos && controller2.getTriggerAxis(Hand.kLeft)<.1){
      lift.set(ControlMode.PercentOutput, .1);
      prevPos = lift.getSelectedSensorPosition();
    }else{
      lift.set(ControlMode.PercentOutput, 0);
    }

    if(controller2.getTriggerAxis(Hand.kRight)>.1){
    
      lift.set(ControlMode.PercentOutput, controller2.getTriggerAxis(Hand.kRight));
    
    }else{
    
      lift.set(ControlMode.PercentOutput, 0);
    
    }

  }
  private void winch() {
    if(controller.getBumper(Hand.kRight)){
      winch.set(1);
    }else if(controller.getBumper(Hand.kLeft)){
      winch.set(-.5);
    }else{
      winch.set(0);
    }
  }










}